module gitlab.com/BIC_Dev/status-bot-v2

go 1.14

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gammazero/workerpool v1.1.1
	github.com/go-openapi/runtime v0.19.26
	github.com/go-openapi/strfmt v0.20.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/mediocregopher/radix/v3 v3.7.0
	gitlab.com/BIC_Dev/guild-config-service-client v0.7.1
	gitlab.com/BIC_Dev/nitrado-service-v2-client v1.1.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0

)
