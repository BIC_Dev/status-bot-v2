package models

import "fmt"

// StatusDisplayChannel struct
type StatusDisplayChannel struct {
	Channel Channel `json:"channel"`
}

// CacheKey func
func (cms *StatusDisplayChannel) CacheKey(base, channelID string) string {
	return fmt.Sprintf("%s:%s", base, channelID)
}
