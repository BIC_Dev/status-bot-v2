package models

import "fmt"

// ServerPlayers struct
type ServerPlayers struct {
	Server         Server `json:"server"`
	PlayersCurrent int    `json:"players_current"`
	PlayersMax     int    `json:"players_max"`
}

// CacheKey func
func (cms *ServerPlayers) CacheKey(base string, serverID int) string {
	return fmt.Sprintf("%s:%d", base, serverID)
}
