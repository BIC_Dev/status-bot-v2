package models

import "fmt"

// ChannelNameChanges struct
type ChannelNameChanges struct {
	Channel Channel `json:"channel"`
}

// CacheKey func
func (cms *ChannelNameChanges) CacheKey(base string, channelID string) string {
	return fmt.Sprintf("%s:%s", base, channelID)
}
