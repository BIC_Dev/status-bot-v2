package models

import "fmt"

// ServerRuntime struct
type ServerRuntime struct {
	Server           Server `json:"server"`
	SuspendingIn     int64  `json:"suspending_in"`
	DeletingIn       int64  `json:"deleting_in"`
	SuspendTimestamp int64  `json:"suspend_timestamp"`
	DeleteTimestamp  int64  `json:"delete_timestamp"`
}

// CacheKey func
func (cms *ServerRuntime) CacheKey(base string, serverID uint64) string {
	return fmt.Sprintf("%s:%d", base, serverID)
}
