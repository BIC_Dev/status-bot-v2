package models

import "fmt"

// CommandMessageReaction struct
type CommandMessageReaction struct {
	Command Command `json:"command"`
	// TODO: Consider adding SubCommand (or some other modifier). Can be used for multi-step embed based reaction commands.
	Message  Message    `json:"message"`
	Reaction []Reaction `json:"reactions"`
	User     *User      `json:"user"`
}

// CacheKey func
func (cmr *CommandMessageReaction) CacheKey(base, messageID string) string {
	return fmt.Sprintf("%s:%s", base, messageID)
}
