package models

import "fmt"

// StatusOutputChannelMessages struct
type StatusOutputChannelMessages struct {
	Messages []Message `json:"messages"`
	Channel  Channel   `json:"channel"`
}

// CacheKey func
func (cms *StatusOutputChannelMessages) CacheKey(base, channelID string) string {
	return fmt.Sprintf("%s:%s", base, channelID)
}
