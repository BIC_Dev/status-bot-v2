package models

import "fmt"

// CommandMessageStep struct
type CommandMessageStep struct {
	Command Command `json:"command"`
	Message Message `json:"message"`
	Step    Step    `json:"step"`
	User    User    `json:"user"`
}

// CacheKey func
func (cms *CommandMessageStep) CacheKey(base, userID, channelID string) string {
	return fmt.Sprintf("%s:%s:%s", base, userID, channelID)
}
