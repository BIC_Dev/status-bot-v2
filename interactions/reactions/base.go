package reactions

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/cache"
)

// Reactions struct
type Reactions struct {
	Session                 *discordgo.Session
	Config                  *configs.Config
	Cache                   *cache.Cache
	NitradoServiceToken     string
	GuildConfigServiceToken string
}

// IsCommandReaction func
func (r *Reactions) IsCommandReaction(ctx, messageID string, emojiID string) {

}
