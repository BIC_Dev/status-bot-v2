package messages

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/models"
	"gitlab.com/BIC_Dev/status-bot-v2/services/guildconfigservice"
	"gitlab.com/BIC_Dev/status-bot-v2/services/nitradoservice"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/cache"
)

// Messages struct
type Messages struct {
	Session            *discordgo.Session
	Config             *configs.Config
	Cache              *cache.Cache
	GuildConfigService *guildconfigservice.GuildConfigService
	NitradoService     *nitradoservice.NitradoService
}

// Factory func
func (c *Messages) Factory(s *discordgo.Session, mc *discordgo.MessageCreate, cms *models.CommandMessageStep) {
	switch cms.Command.Name {
	case "nitradotoken":
		// TODO: Call nitrado token command functionality
	default:
		// TODO: Log error
	}
}
