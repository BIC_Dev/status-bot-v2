package commands

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/server_output_channels"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// SetOutputChannelCommand struct
type SetOutputChannelCommand struct {
	Params SetOutputChannelCommandParams
}

// SetOutputChannelCommandParams struct
type SetOutputChannelCommandParams struct {
	ServerID  int64
	ChannelID string
}

// SetOutputChannelOutput struct
type SetOutputChannelOutput struct {
	OldOutputChannelID string
	NewOutputChannelID string
}

// SetOutputChannel func
func (c *Commands) SetOutputChannel(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	setOutputChannelCommand, err := parseSetOutputChannelCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	_, cErr := discordapi.GetChannel(s, setOutputChannelCommand.Params.ChannelID)
	if cErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Error getting output channel",
			Err:     cErr,
		})
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Bot has not been activated",
				Err:     notfound,
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Faild to find bot information",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Enabled == false {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use status bot",
			Err:     errors.New("disabled guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	guildServiceExists := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			guildServiceExists = true
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if !guildServiceExists {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use this bot",
			Err:     errors.New("no guild service"),
		})
		return
	}

	if guildFeed.Payload.Guild.Servers == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has no linked servers",
			Err:     errors.New("guild feed nil servers"),
		})
		return
	}

	var server *gcscmodels.Server
	for _, aServer := range guildFeed.Payload.Guild.Servers {
		if aServer.NitradoID == setOutputChannelCommand.Params.ServerID {
			server = aServer
		}
	}

	if server == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Server ID not found",
			Err:     errors.New("invalid server id"),
		})
		return
	}

	var existingOutputChannel *gcscmodels.ServerOutputChannel
	if server.ServerOutputChannels != nil {
		for _, outputChannel := range server.ServerOutputChannels {
			if outputChannel.OutputChannelTypeID != "status-output" {
				continue
			}

			if outputChannel.ChannelID == setOutputChannelCommand.Params.ChannelID {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Output channel already set",
					Err:     errors.New("already set with same channel"),
				})
				return
			}

			existingOutputChannel = outputChannel
			break
		}
	}

	if existingOutputChannel != nil {
		body := gcscmodels.UpdateServerOutputChannelRequest{
			ChannelID:           setOutputChannelCommand.Params.ChannelID,
			Enabled:             true,
			OutputChannelTypeID: existingOutputChannel.OutputChannelTypeID,
			ServerID:            server.ID,
		}
		outputChannelParams := server_output_channels.NewUpdateServerOutputChannelParamsWithTimeout(10)
		outputChannelParams.Guild = mc.GuildID
		outputChannelParams.ServerOutputChannelID = int64(existingOutputChannel.ID)
		outputChannelParams.Context = context.Background()
		outputChannelParams.Body = &body
		_, ocErr := c.GuildConfigService.Client.ServerOutputChannels.UpdateServerOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
		if ocErr != nil {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Failed to update output channel",
				Err:     ocErr,
			})
			return
		}

		var embeddableFields []discordapi.EmbeddableField
		var embeddableErrors []discordapi.EmbeddableField
		setOutputChannelOutput := SetOutputChannelOutput{
			OldOutputChannelID: existingOutputChannel.ChannelID,
			NewOutputChannelID: setOutputChannelCommand.Params.ChannelID,
		}
		embeddableFields = append(embeddableFields, &setOutputChannelOutput)

		SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
		return
	}

	body := gcscmodels.ServerOutputChannel{
		ChannelID:           setOutputChannelCommand.Params.ChannelID,
		Enabled:             true,
		OutputChannelTypeID: "status-output",
		ServerID:            server.ID,
	}
	outputChannelParams := server_output_channels.NewCreateServerOutputChannelParamsWithTimeout(10)
	outputChannelParams.Guild = mc.GuildID
	outputChannelParams.Context = context.Background()
	outputChannelParams.Body = &body
	_, ocErr := c.GuildConfigService.Client.ServerOutputChannels.CreateServerOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
	if ocErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to add output channel",
			Err:     ocErr,
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField
	setOutputChannelOutput := SetOutputChannelOutput{
		NewOutputChannelID: setOutputChannelCommand.Params.ChannelID,
	}
	embeddableFields = append(embeddableFields, &setOutputChannelOutput)

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
	return
}

// parseSetOutputChannelCommand func
func parseSetOutputChannelCommand(command configs.Command, mc *discordgo.MessageCreate) (*SetOutputChannelCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	serverIDInt, sidErr := strconv.ParseInt(splitContent[1], 10, 64)
	if sidErr != nil {
		return nil, &Error{
			Message: "Invalid Server ID",
			Err:     sidErr,
		}
	}

	channelID := ""
	if len(mc.MentionChannels) > 0 {
		for _, channel := range mc.MentionChannels {
			channelID = channel.ID
			break
		}
	} else if !strings.HasPrefix(splitContent[2], "<#") || !strings.HasSuffix(splitContent[2], ">") {
		return nil, &Error{
			Message: "Invalid Channel",
			Err:     errors.New("invalid channel"),
		}
	} else {
		channelReference := splitContent[2]
		channelLength := len(channelReference)
		channelID = channelReference[2 : channelLength-1]
	}

	return &SetOutputChannelCommand{
		Params: SetOutputChannelCommandParams{
			ServerID:  serverIDInt,
			ChannelID: channelID,
		},
	}, nil
}

// ConvertToEmbedField for Error struct
func (soo *SetOutputChannelOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""
	if soo.OldOutputChannelID == "" {
		fieldVal += "Old Channel: none\n"
	} else {
		fieldVal += fmt.Sprintf("Old Channel: <#%s>\n", soo.OldOutputChannelID)
	}

	fieldVal += fmt.Sprintf("New Channel: <#%s>", soo.NewOutputChannelID)

	return &discordgo.MessageEmbedField{
		Name:   "Changed Output Channel",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
