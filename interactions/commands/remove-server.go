package commands

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/servers"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// RemoveServerCommand struct
type RemoveServerCommand struct {
	Params RemoveServerParams
}

// RemoveServerParams struct
type RemoveServerParams struct {
	ServerID int64
}

// RemoveServerOutput struct
type RemoveServerOutput struct {
	Server gcscmodels.Server
}

// RemoveServer func
func (c *Commands) RemoveServer(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	removeServerCommand, rscErr := parseRemoveServerCommand(command, mc)
	if rscErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *rscErr)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)
	if gfErr != nil {
		if val, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); !ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to get guild feed",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	guildServiceExists := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			guildServiceExists = true
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if !guildServiceExists {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use this bot",
			Err:     errors.New("no guild service"),
		})
		return
	}

	if guildFeed.Payload.Guild.Servers == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil servers"),
		})
		return
	}

	var oldServer *gcscmodels.Server
	for _, server := range guildFeed.Payload.Guild.Servers {
		if server.NitradoID == removeServerCommand.Params.ServerID {
			oldServer = server
			break
		}
	}

	if oldServer == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Server does not exist with ID",
			Err:     errors.New("invalid server id"),
		})
		return
	}

	serverParams := servers.NewDeleteServerParamsWithTimeout(10)
	serverParams.Guild = mc.GuildID
	serverParams.ServerID = int64(oldServer.ID)
	serverParams.Context = context.Background()

	_, srErr := c.GuildConfigService.Client.Servers.DeleteServer(serverParams, c.GuildConfigService.Auth)
	if srErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to remove server",
			Err:     srErr,
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField
	embeddableFields = append(embeddableFields, &RemoveServerOutput{
		Server: *oldServer,
	})

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
}

// parseRemoveServerCommand func
func parseRemoveServerCommand(command configs.Command, mc *discordgo.MessageCreate) (*RemoveServerCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	serverIDInt, sidErr := strconv.ParseInt(splitContent[1], 10, 64)
	if sidErr != nil {
		return nil, &Error{
			Message: "Invalid Server ID",
			Err:     sidErr,
		}
	}

	return &RemoveServerCommand{
		Params: RemoveServerParams{
			ServerID: serverIDInt,
		},
	}, nil
}

// ConvertToEmbedField for NameServerOutput struct
func (rso *RemoveServerOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("```Removed Server Name:\n\t%s\nRemoved Server ID:\n\t%d```", rso.Server.Name, rso.Server.NitradoID)
	return &discordgo.MessageEmbedField{
		Name:   "Removed Server",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
