package commands

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// HelpCommand struct
type HelpCommand struct {
	Params HelpCommandParams
}

// HelpCommandParams struct
type HelpCommandParams struct {
}

// HelpOutput struct
type HelpOutput struct {
	Command configs.Command `json:"command"`
	Prefix  string          `json:"prefix"`
}

// Help func
func (c *Commands) Help(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	_, err := parseHelpCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	var commands []configs.Command = c.Config.Commands

	sort.SliceStable(commands, func(i, j int) bool {
		return commands[i].Name < commands[j].Name
	})

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	for _, aCommand := range c.Config.Commands {
		if aCommand.Name == "Help" {
			continue
		}

		if !aCommand.Enabled {
			continue
		}

		embeddableFields = append(embeddableFields, &HelpOutput{
			Command: aCommand,
			Prefix:  c.Config.Bot.Prefix,
		})
	}

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
	return
}

// parseHelpCommand func
func parseHelpCommand(command configs.Command, mc *discordgo.MessageCreate) (*HelpCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	return &HelpCommand{
		Params: HelpCommandParams{},
	}, nil
}

// ConvertToEmbedField for Help struct
func (h *HelpOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	usages := ""
	for _, usage := range h.Command.Usage {
		if usages == "" {
			usages = "\t" + h.Prefix + usage
		} else {
			usages += "\n\t" + h.Prefix + usage
		}
	}
	examples := ""
	for _, example := range h.Command.Examples {
		if examples == "" {
			examples = "\t" + h.Prefix + example
		} else {
			examples += "\n\t" + h.Prefix + example
		}
	}
	return &discordgo.MessageEmbedField{
		Name:   h.Command.Name,
		Value:  fmt.Sprintf("%s\n```USAGE:\n%s\nEXAMPLES:\n%s```", h.Command.Description, usages, examples),
		Inline: false,
	}, nil
}
