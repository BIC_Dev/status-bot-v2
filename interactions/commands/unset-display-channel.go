package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_output_channels"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// UnsetDisplayChannelCommand struct
type UnsetDisplayChannelCommand struct {
	Params UnsetDisplayChannelCommandParams
}

// UnsetDisplayChannelCommandParams struct
type UnsetDisplayChannelCommandParams struct {
	ChannelID string
}

// UnsetDisplayChannelOutput struct
type UnsetDisplayChannelOutput struct {
	Channel *discordgo.Channel
}

// UnsetDisplayChannel func
func (c *Commands) UnsetDisplayChannel(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	unsetDisplayChannelCommand, err := parseUnsetDisplayChannelCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Bot has not been activated",
				Err:     notfound,
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Faild to find bot information",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Enabled == false {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use status bot",
			Err:     errors.New("disabled guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	var existingOutputChannel *gcscmodels.GuildOutputChannel
	if guildFeed.Payload.Guild.GuildOutputChannels != nil {
		for _, outputChannel := range guildFeed.Payload.Guild.GuildOutputChannels {
			switch outputChannel.OutputChannelTypeID {
			case "status-players-display":
			case "status-servers-display":
			case "status-runtime-display":
			default:
				continue
			}

			if outputChannel.ChannelID == unsetDisplayChannelCommand.Params.ChannelID {
				existingOutputChannel = outputChannel
				break
			}
		}
	}

	if existingOutputChannel == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Cannot unset a display chanel not in use",
			Err:     errors.New("invalid channel"),
		})
		return
	}

	outputChannelParams := guild_output_channels.NewDeleteGuildOutputChannelParamsWithTimeout(10)
	outputChannelParams.SetGuild(mc.GuildID)
	outputChannelParams.SetGuildOutputChannelID(int64(existingOutputChannel.ID))
	outputChannelParams.SetContext(context.Background())
	_, ocErr := c.GuildConfigService.Client.GuildOutputChannels.DeleteGuildOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
	if ocErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to unset display channel",
			Err:     ocErr,
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	channel, gcErr := discordapi.GetChannel(s, unsetDisplayChannelCommand.Params.ChannelID)
	if gcErr != nil {
		embeddableFields = append(embeddableFields, &UnsetDisplayChannelOutput{})
		SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
		return
	}

	_, dcErr := discordapi.DeleteChannel(s, unsetDisplayChannelCommand.Params.ChannelID)
	if dcErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to delete display channel",
			Err:     dcErr,
		})
		return
	}

	embeddableFields = append(embeddableFields, &UnsetDisplayChannelOutput{
		Channel: channel,
	})
	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
}

// parseUnsetDisplayChannelCommand func
func parseUnsetDisplayChannelCommand(command configs.Command, mc *discordgo.MessageCreate) (*UnsetDisplayChannelCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	channelID := ""
	if len(mc.MentionChannels) > 0 {
		for _, channel := range mc.MentionChannels {
			channelID = channel.ID
			break
		}
	} else if !strings.HasPrefix(splitContent[1], "<#") || !strings.HasSuffix(splitContent[1], ">") {
		return nil, &Error{
			Message: "Invalid Channel",
			Err:     errors.New("invalid channel"),
		}
	} else {
		channelReference := splitContent[1]
		channelLength := len(channelReference)
		channelID = channelReference[2 : channelLength-1]
	}

	return &UnsetDisplayChannelCommand{
		Params: UnsetDisplayChannelCommandParams{
			ChannelID: channelID,
		},
	}, nil
}

// ConvertToEmbedField for Error struct
func (o *UnsetDisplayChannelOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""
	if o.Channel != nil {
		fieldVal += "Removed: " + o.Channel.Name
	} else {
		fieldVal += "Channel previously deleted"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Unset Display Channel",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
