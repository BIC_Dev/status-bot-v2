package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// ListServersCommand struct
type ListServersCommand struct {
	Params ListServersCommandParams
}

// ListServersCommandParams struct
type ListServersCommandParams struct{}

// ListServersOutput struct
type ListServersOutput struct {
	gcscmodels.Server
}

// ListDisplayChannelsOutput struct
type ListDisplayChannelsOutput struct {
	DisplayChannels []*gcscmodels.GuildOutputChannel
}

// ListServers func
func (c *Commands) ListServers(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	_, err := parseListServersCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)
	if gfErr != nil {
		if val, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to get guild feed",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Servers == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil servers"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	guildServiceExists := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			guildServiceExists = true
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if !guildServiceExists {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use this bot",
			Err:     errors.New("no guild service"),
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	if guildFeed.Payload.Guild.GuildOutputChannels != nil {
		listDisplayChannelsOutput := ListDisplayChannelsOutput{
			DisplayChannels: guildFeed.Payload.Guild.GuildOutputChannels,
		}
		embeddableFields = append(embeddableFields, &listDisplayChannelsOutput)
	}

	for _, server := range guildFeed.Payload.Guild.Servers {
		var aServer = *server
		embeddableFields = append(embeddableFields, &ListServersOutput{
			aServer,
		})
	}

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
}

// parseListServersCommand func
func parseListServersCommand(command configs.Command, mc *discordgo.MessageCreate) (*ListServersCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	return &ListServersCommand{
		Params: ListServersCommandParams{},
	}, nil
}

// ConvertToEmbedField for ListServersOutput struct
func (lso *ListServersOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("**ID:** %d", lso.NitradoID)

	if lso.BoostSetting != nil {
		fieldVal += fmt.Sprintf("\n**Boost Code:** %s", lso.BoostSetting.Code)
	}

	var statusDisplayChannels []string
	var statusOutputChannels []string
	if lso.ServerOutputChannels != nil {
		for _, outputChannel := range lso.ServerOutputChannels {
			if outputChannel.OutputChannelType == nil {
				continue
			}

			switch outputChannel.OutputChannelType.ID {
			case "status-display":
				statusDisplayChannels = append(statusDisplayChannels, outputChannel.ChannelID)
			case "status-output":
				statusOutputChannels = append(statusOutputChannels, outputChannel.ChannelID)
			}
		}
	}

	if len(statusDisplayChannels) > 0 || len(statusOutputChannels) > 0 {
		fieldVal += "\n**Channels**"
	}

	for _, channelID := range statusDisplayChannels {
		fieldVal += fmt.Sprintf("\n  **Display:** <#%s>", channelID)
	}

	for _, channelID := range statusOutputChannels {
		fieldVal += fmt.Sprintf("\n  **Output:** <#%s>", channelID)
	}

	return &discordgo.MessageEmbedField{
		Name:   lso.Name,
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for ListDisplayChannelsOutput struct
func (lso *ListDisplayChannelsOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	for _, displayChannel := range lso.DisplayChannels {
		outputChannelType := ""
		switch displayChannel.OutputChannelTypeID {
		case "status-players-display":
			outputChannelType = "Players"
		case "status-servers-display":
			outputChannelType = "Servers"
		case "status-runtime-display":
			outputChannelType = "Runtime"
		default:
			continue
		}
		fieldVal += fmt.Sprintf("**%s**: <#%s>\n", outputChannelType, displayChannel.ChannelID)
	}

	return &discordgo.MessageEmbedField{
		Name:   "Display Channels",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
