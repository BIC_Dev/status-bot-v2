package commands

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/servers"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// NameServerCommand struct
type NameServerCommand struct {
	Params NameServerParams
}

// NameServerParams struct
type NameServerParams struct {
	ServerID int64
	Name     string
}

// NameServerOutput struct
type NameServerOutput struct {
	ServerOld gcscmodels.Server
	ServerNew gcscmodels.Server
}

// NameServer func
func (c *Commands) NameServer(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	nameServerCommand, nscErr := parseNameServerCommand(command, mc)
	if nscErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *nscErr)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)
	if gfErr != nil {
		if val, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); !ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to get guild feed",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Servers == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to retrieve bot information",
			Err:     errors.New("guild feed has nil servers"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	guildServiceExists := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			guildServiceExists = true
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if !guildServiceExists {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use this bot",
			Err:     errors.New("no guild service"),
		})
		return
	}

	var oldServer *gcscmodels.Server
	for _, server := range guildFeed.Payload.Guild.Servers {
		if server.NitradoID == nameServerCommand.Params.ServerID {
			oldServer = server
			break
		}
	}

	if oldServer == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Server does not exist with ID",
			Err:     errors.New("invalid server id"),
		})
		return
	}

	serverParamsBody := gcscmodels.UpdateServerRequest{
		BoostSettingID: oldServer.BoostSettingID,
		Enabled:        oldServer.Enabled,
		GuildID:        oldServer.GuildID,
		Name:           nameServerCommand.Params.Name,
		NitradoID:      oldServer.NitradoID,
		NitradoTokenID: oldServer.NitradoTokenID,
		ServerTypeID:   oldServer.ServerTypeID,
	}
	serverParams := servers.NewUpdateServerParamsWithTimeout(10)
	serverParams.Guild = mc.GuildID
	serverParams.ServerID = int64(oldServer.ID)
	serverParams.Context = context.Background()
	serverParams.Body = &serverParamsBody

	serverResponse, srErr := c.GuildConfigService.Client.Servers.UpdateServer(serverParams, c.GuildConfigService.Auth)
	if srErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to update server name",
			Err:     srErr,
		})
		return
	}

	if serverResponse.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to update server name",
			Err:     errors.New("server update has nil payload"),
		})
		return
	}

	if serverResponse.Payload.Server == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to update server name",
			Err:     errors.New("server update has nil server"),
		})
		return
	}

	newServer := serverResponse.Payload.Server

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField
	embeddableFields = append(embeddableFields, &NameServerOutput{
		ServerOld: *oldServer,
		ServerNew: *newServer,
	})

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
}

// parseNameServerCommand func
func parseNameServerCommand(command configs.Command, mc *discordgo.MessageCreate) (*NameServerCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	serverIDInt, sidErr := strconv.ParseInt(splitContent[1], 10, 64)
	if sidErr != nil {
		return nil, &Error{
			Message: "Invalid Server ID",
			Err:     sidErr,
		}
	}

	return &NameServerCommand{
		Params: NameServerParams{
			ServerID: serverIDInt,
			Name:     strings.Join(splitContent[2:], " "),
		},
	}, nil
}

// ConvertToEmbedField for NameServerOutput struct
func (nso *NameServerOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("```Old Server Name:\n\t%s\nNew Server Name:\n\t%s```", nso.ServerOld.Name, nso.ServerNew.Name)
	return &discordgo.MessageEmbedField{
		Name:   "Updated Server Name",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
