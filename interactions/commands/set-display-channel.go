package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_output_channels"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// SetDisplayChannelCommand struct
type SetDisplayChannelCommand struct {
	Params SetDisplayChannelCommandParams
}

// SetDisplayChannelCommandParams struct
type SetDisplayChannelCommandParams struct {
	ChannelType    string
	APIChannelType string
	ChannelID      string
}

// SetDisplayChannelOutput struct
type SetDisplayChannelOutput struct {
	OldOutputChannelID string
	NewOutputChannelID string
}

// SetDisplayChannel func
func (c *Commands) SetDisplayChannel(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	setDisplayChannelCommand, err := parseSetDisplayChannelCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	_, cErr := discordapi.GetChannel(s, setDisplayChannelCommand.Params.ChannelID)
	if cErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Error getting display channel",
			Err:     cErr,
		})
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Bot has not been activated",
				Err:     notfound,
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Faild to find bot information",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Enabled == false {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use status bot",
			Err:     errors.New("disabled guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	guildServiceExists := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			guildServiceExists = true
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if !guildServiceExists {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use this bot",
			Err:     errors.New("no guild service"),
		})
		return
	}

	var existingOutputChannel *gcscmodels.GuildOutputChannel
	if guildFeed.Payload.Guild.GuildOutputChannels != nil {
		for _, outputChannel := range guildFeed.Payload.Guild.GuildOutputChannels {
			switch outputChannel.OutputChannelTypeID {
			case "status-players-display":
			case "status-servers-display":
			case "status-runtime-display":
			default:
				continue
			}

			if outputChannel.ChannelID == setDisplayChannelCommand.Params.ChannelID {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Display channel already set",
					Err:     errors.New("already set with same channel"),
				})
				return
			}

			if outputChannel.OutputChannelTypeID == setDisplayChannelCommand.Params.APIChannelType {
				existingOutputChannel = outputChannel
				break
			}
		}
	}

	if existingOutputChannel != nil {
		body := gcscmodels.UpdateGuildOutputChannelRequest{
			ChannelID:           setDisplayChannelCommand.Params.ChannelID,
			Enabled:             true,
			OutputChannelTypeID: existingOutputChannel.OutputChannelTypeID,
			GuildID:             mc.GuildID,
		}
		outputChannelParams := guild_output_channels.NewUpdateGuildOutputChannelParamsWithTimeout(10)
		outputChannelParams.Guild = mc.GuildID
		outputChannelParams.GuildOutputChannelID = int64(existingOutputChannel.ID)
		outputChannelParams.Context = context.Background()
		outputChannelParams.Body = &body
		_, ocErr := c.GuildConfigService.Client.GuildOutputChannels.UpdateGuildOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
		if ocErr != nil {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Failed to update display channel",
				Err:     ocErr,
			})
			return
		}

		var embeddableFields []discordapi.EmbeddableField
		var embeddableErrors []discordapi.EmbeddableField
		setOutputChannelOutput := SetDisplayChannelOutput{
			OldOutputChannelID: existingOutputChannel.ChannelID,
			NewOutputChannelID: setDisplayChannelCommand.Params.ChannelID,
		}
		embeddableFields = append(embeddableFields, &setOutputChannelOutput)

		SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
		return
	}

	body := gcscmodels.GuildOutputChannel{
		ChannelID:           setDisplayChannelCommand.Params.ChannelID,
		Enabled:             true,
		OutputChannelTypeID: setDisplayChannelCommand.Params.APIChannelType,
		GuildID:             mc.GuildID,
	}
	outputChannelParams := guild_output_channels.NewCreateGuildOutputChannelParamsWithTimeout(10)
	outputChannelParams.Guild = mc.GuildID
	outputChannelParams.Context = context.Background()
	outputChannelParams.Body = &body
	_, ocErr := c.GuildConfigService.Client.GuildOutputChannels.CreateGuildOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
	if ocErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to add display channel",
			Err:     ocErr,
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField
	setOutputChannelOutput := SetDisplayChannelOutput{
		NewOutputChannelID: setDisplayChannelCommand.Params.ChannelID,
	}
	embeddableFields = append(embeddableFields, &setOutputChannelOutput)

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
	return
}

// parseSetDisplayChannelCommand func
func parseSetDisplayChannelCommand(command configs.Command, mc *discordgo.MessageCreate) (*SetDisplayChannelCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	channelID := ""
	if len(mc.MentionChannels) > 0 {
		for _, channel := range mc.MentionChannels {
			channelID = channel.ID
			break
		}
	} else if !strings.HasPrefix(splitContent[2], "<#") || !strings.HasSuffix(splitContent[2], ">") {
		return nil, &Error{
			Message: "Invalid Channel",
			Err:     errors.New("invalid channel"),
		}
	} else {
		channelReference := splitContent[2]
		channelLength := len(channelReference)
		channelID = channelReference[2 : channelLength-1]
	}

	apiChannelType := ""
	switch splitContent[1] {
	case "players":
		apiChannelType = "status-players-display"
	case "servers":
		apiChannelType = "status-servers-display"
	case "runtime":
		apiChannelType = "status-runtime-display"
	default:
		return nil, &Error{
			Message: "Invalid Channel Type",
			Err:     errors.New("invalid channel type"),
		}
	}

	return &SetDisplayChannelCommand{
		Params: SetDisplayChannelCommandParams{
			ChannelType:    splitContent[1],
			APIChannelType: apiChannelType,
			ChannelID:      channelID,
		},
	}, nil
}

// ConvertToEmbedField for Error struct
func (soo *SetDisplayChannelOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""
	if soo.OldOutputChannelID == "" {
		fieldVal += "Old Channel: none\n"
	} else {
		fieldVal += fmt.Sprintf("Old Channel: <#%s>\n", soo.OldOutputChannelID)
	}

	fieldVal += fmt.Sprintf("New Channel: <#%s>", soo.NewOutputChannelID)

	return &discordgo.MessageEmbedField{
		Name:   "Changed Display Channel",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
