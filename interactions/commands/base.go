package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/services/guildconfigservice"
	"gitlab.com/BIC_Dev/status-bot-v2/services/nitradoservice"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/cache"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// Commands struct
type Commands struct {
	Session            *discordgo.Session
	Config             *configs.Config
	Cache              *cache.Cache
	GuildConfigService *guildconfigservice.GuildConfigService
	NitradoService     *nitradoservice.NitradoService
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
}

// Error func
func (e *Error) Error() string {
	return e.Err.Error()
}

// HasPrefix func
func (c *Commands) HasPrefix(ctx, message string) bool {
	prefixLen := len(c.Config.Bot.Prefix)
	if len(message) < prefixLen {
		return false
	}

	if strings.ToLower(message[:prefixLen]) != c.Config.Bot.Prefix {
		return false
	}

	return true
}

// getCommand func
func getCommand(commands []configs.Command, prefix string, content string) (configs.Command, *Error) {
	prefixLen := len(prefix)
	if len(content) <= prefixLen {
		return configs.Command{}, &Error{
			Message: "Not a command",
			Err:     errors.New("not a command"),
		}
	}

	return getCommandConfig(commands, strings.SplitN(content[prefixLen:], " ", 2)[0])

}

// Factory func
func (c *Commands) Factory(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("message_id", mc.ID),
		zap.String("guild_id", mc.GuildID),
		zap.String("channel_id", mc.ChannelID),
		zap.String("user_id", mc.Author.ID),
		zap.String("message", mc.Content),
	)

	command, gcErr := getCommand(c.Config.Commands, c.Config.Bot.Prefix, mc.Content)
	if gcErr != nil {
		if gcErr.Error() == "not a command" {
			return
		}

		ctx = logging.AddValues(ctx, zap.NamedError("error", gcErr.Err), zap.String("error_message", gcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	logger := logging.Logger(ctx)
	logger.Info("command_log")

	switch command.Name {
	case "Add Nitrado Token":
		break
	default:
		if mc.GuildID == "" {
			ErrorOutput(ctx, c.Session, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "This command cannot be used through DM",
				Err:     errors.New("must be used in discord server"),
			})
			return
		}
		isAdmin, iaErr := c.IsAdmin(ctx, mc.GuildID, mc.Member.Roles)
		if iaErr != nil {
			ErrorOutput(ctx, c.Session, c.Config, command, mc.Content, mc.ChannelID, *iaErr)
			return
		}
		if !isAdmin {
			ErrorOutput(ctx, c.Session, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Unauthorized to use this command",
				Err:     errors.New("user is not administrator"),
			})
			return
		}
	}

	switch command.Name {
	case "Set Display Channel":
		c.SetDisplayChannel(ctx, s, mc, command)
	case "Unset Display Channel":
		c.UnsetDisplayChannel(ctx, s, mc, command)
	case "List Servers":
		c.ListServers(ctx, s, mc, command)
	case "Name Server":
		c.NameServer(ctx, s, mc, command)
	case "Nitrado Token":
		c.NitradoToken(ctx, s, mc, command)
	case "Add Nitrado Token":
		c.AddNitradoToken(ctx, s, mc, command)
	case "Set Output Channel":
		c.SetOutputChannel(ctx, s, mc, command)
	case "Unset Output Channel":
		c.UnsetOutputChannel(ctx, s, mc, command)
	case "Remove Server":
		c.RemoveServer(ctx, s, mc, command)
	case "Auto Setup":
		c.Setup(ctx, s, mc, command)
	case "Bot Activation":
		c.Activate(ctx, s, mc, command)
	case "Help":
		c.Help(ctx, s, mc, command)
	default:
		// TODO: Output error
	}
}

// getCommandConfig func
func getCommandConfig(commands []configs.Command, command string) (configs.Command, *Error) {
	for _, val := range commands {
		if val.Long == strings.ToLower(command) || val.Short == strings.ToLower(command) {
			return val, nil
		}
	}

	return configs.Command{}, &Error{
		Message: fmt.Sprintf("No command found with name: %s", command),
		Err:     errors.New("invalid command"),
	}
}

// ErrorOutput func
func ErrorOutput(ctx context.Context, session *discordgo.Session, c *configs.Config, command configs.Command, content string, channelID string, err Error) ([]*discordgo.Message, *Error) {
	newCtx := logging.AddValues(ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Message))
	logger := logging.Logger(newCtx)
	logger.Error("error_log")

	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params := discordapi.EmbeddableParams{
		Title:       "Error",
		Description: "`" + content + "`",
		Color:       c.Bot.ErrorColor,
		TitleURL:    c.Bot.DocumentationURL,
		Footer:      "Error",
	}

	var embeddableFields []discordapi.EmbeddableField

	embeddableFields = append(embeddableFields, &err)
	embeddableFields = append(embeddableFields, &HelpOutput{
		Command: command,
		Prefix:  c.Bot.Prefix,
	})

	embeds := discordapi.CreateEmbeds(params, embeddableFields)

	var messages []*discordgo.Message
	for _, embed := range embeds {
		message, smErr := discordapi.SendMessage(session, channelID, nil, &embed)
		if smErr != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", smErr.Err), zap.String("error_message", smErr.Message), zap.Int("status_code", smErr.Code))
			logger := logging.Logger(ctx)
			logger.Error("error_log")

			return nil, &Error{
				Message: smErr.Message,
				Err:     smErr.Err,
			}
		}
		messages = append(messages, message)
	}

	return messages, nil
}

// SuccessOutput func
func SuccessOutput(ctx context.Context, session *discordgo.Session, c *configs.Config, channelID string, command configs.Command, embeddableFields []discordapi.EmbeddableField, embeddableErrors []discordapi.EmbeddableField) ([]*discordgo.Message, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	params := discordapi.EmbeddableParams{
		Title:       command.Name,
		Description: command.Description,
		Color:       c.Bot.OkColor,
		TitleURL:    c.Bot.DocumentationURL,
		Footer:      "Executed",
	}

	if len(embeddableErrors) > 0 {
		params.Color = c.Bot.WarnColor
	}

	combinedFields := append(embeddableFields, embeddableErrors...)
	embeds := discordapi.CreateEmbeds(params, combinedFields)

	var messages []*discordgo.Message
	for _, embed := range embeds {
		message, err := discordapi.SendMessage(session, channelID, nil, &embed)
		if err != nil {
			ctx = logging.AddValues(ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Message), zap.Int("status_code", err.Code))
			logger := logging.Logger(ctx)
			logger.Error("error_log")

			return nil, &Error{
				Message: err.Message,
				Err:     err.Err,
			}
		}
		messages = append(messages, message)
	}

	return messages, nil
}

// ConvertToEmbedField for Error struct
func (e *Error) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	return &discordgo.MessageEmbedField{
		Name:   e.Message,
		Value:  e.Error(),
		Inline: false,
	}, nil
}

// IsAdmin func
func (c *Commands) IsAdmin(ctx context.Context, guildID string, roles []string) (bool, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	discRoles, grErr := discordapi.GetGuildRoles(c.Session, guildID)
	if grErr != nil {
		return false, &Error{
			Message: "Failed to get Guild roles to verify Administrator access",
			Err:     grErr.Err,
		}
	}

	for _, memberRole := range roles {
		for _, guildRole := range discRoles {
			if memberRole == guildRole.ID && (guildRole.Permissions&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator) {
				return true, nil
			}
		}
	}

	return false, nil
}
