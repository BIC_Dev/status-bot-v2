package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/nitrado_setups"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// SetupCommand struct
type SetupCommand struct {
	Params SetupCommandParams
}

// SetupCommandParams struct
type SetupCommandParams struct{}

// SetupOutput struct
type SetupOutput struct {
	NitradoSetup gcscmodels.CreateNitradoSetupResponse
}

// Setup func
func (c *Commands) Setup(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	_, scErr := parseSetupCommand(command, mc)
	if scErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *scErr)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Bot has not been activated",
				Err:     notfound,
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Faild to find bot information",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Enabled == false {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use status bot",
			Err:     errors.New("disabled guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	dGuild, dgErr := s.Guild(mc.GuildID)
	if dgErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", dgErr), zap.String("error_message", "unable to get Discord server information"))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Unable to get Discord server information",
			Err:     dgErr,
		})
		return
	}

	nitradoSetupBody := gcscmodels.CreateNitradoSetupRequest{
		GuildID:          mc.GuildID,
		GuildName:        dGuild.Name,
		ContactID:        mc.Author.ID,
		ContactName:      mc.Author.Username,
		GuildServiceName: c.Config.Bot.GuildService,
	}
	createNitradoSetupParams := nitrado_setups.NewCreateNitradoSetupParamsWithTimeout(300)
	createNitradoSetupParams.SetContext(context.Background())
	createNitradoSetupParams.SetGuild(mc.GuildID)
	createNitradoSetupParams.SetBody(&nitradoSetupBody)
	nitradoSetupResponse, cnsErr := c.GuildConfigService.Client.NitradoSetups.CreateNitradoSetup(createNitradoSetupParams, c.GuildConfigService.Auth)
	if cnsErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", cnsErr), zap.String("error_message", "unable to create nitrado setup"))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to setup status bot v2",
			Err:     errors.New("failed bot setup"),
		})
		return
	}

	setupOutput := SetupOutput{
		NitradoSetup: *nitradoSetupResponse.Payload,
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	embeddableFields = append(embeddableFields, &setupOutput)

	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
	return
}

// parseListServersCommand func
func parseSetupCommand(command configs.Command, mc *discordgo.MessageCreate) (*SetupCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	return &SetupCommand{
		Params: SetupCommandParams{},
	}, nil
}

// ConvertToEmbedField for Error struct
func (so *SetupOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("Nitrado Accounts: %d\nNew Servers: %d", len(so.NitradoSetup.NitradoTokens), len(so.NitradoSetup.Servers))

	return &discordgo.MessageEmbedField{
		Name:   "Successful Setup",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
