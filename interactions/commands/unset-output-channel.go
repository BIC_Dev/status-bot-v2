package commands

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/server_output_channels"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// UnsetOutputChannelCommand struct
type UnsetOutputChannelCommand struct {
	Params UnsetOutputChannelCommandParams
}

// UnsetOutputChannelCommandParams struct
type UnsetOutputChannelCommandParams struct {
	ServerID int64
}

// UnsetOutputChannelOutput struct
type UnsetOutputChannelOutput struct {
	Server  *gcscmodels.Server
	Channel *discordgo.Channel
}

// UnsetOutputChannel func
func (c *Commands) UnsetOutputChannel(ctx context.Context, s *discordgo.Session, mc *discordgo.MessageCreate, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	unsetOutputChannelCommand, err := parseUnsetOutputChannelCommand(command, mc)
	if err != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, *err)
		return
	}

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = mc.GuildID
	guildFeedParams.GuildID = mc.GuildID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
				Message: "Bot has not been activated",
				Err:     notfound,
			})
			return
		}

		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Faild to find bot information",
			Err:     gfErr,
		})
		return
	}

	if guildFeed.Payload == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil payload"),
		})
		return
	}

	if guildFeed.Payload.Guild == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to find bot information",
			Err:     errors.New("guild feed nil guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.Enabled == false {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Discord server is not enabled to use status bot",
			Err:     errors.New("disabled guild"),
		})
		return
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has not been activated",
			Err:     errors.New("guild feed nil guild services"),
		})
		return
	}

	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == c.Config.Bot.GuildService {
			if guildService.Enabled == false {
				ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
					Message: "Discord server is not enabled to use this bot",
					Err:     errors.New("disabled guild service"),
				})
				return
			}
		}
	}

	if guildFeed.Payload.Guild.Servers == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Bot has no linked servers",
			Err:     errors.New("guild feed nil servers"),
		})
		return
	}

	var server *gcscmodels.Server
	for _, aServer := range guildFeed.Payload.Guild.Servers {
		if aServer.NitradoID == unsetOutputChannelCommand.Params.ServerID {
			server = aServer
		}
	}

	if server == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Server ID not found",
			Err:     errors.New("invalid server id"),
		})
		return
	}

	var existingOutputChannel *gcscmodels.ServerOutputChannel
	if server.ServerOutputChannels != nil {
		for _, outputChannel := range server.ServerOutputChannels {
			if outputChannel.OutputChannelTypeID != "status-output" {
				continue
			}

			existingOutputChannel = outputChannel
			break
		}
	}

	if existingOutputChannel == nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Server does not output to a channel",
			Err:     errors.New("invalid server"),
		})
		return
	}

	outputChannelParams := server_output_channels.NewDeleteServerOutputChannelParamsWithTimeout(10)
	outputChannelParams.SetGuild(mc.GuildID)
	outputChannelParams.SetServerOutputChannelID(int64(existingOutputChannel.ID))
	outputChannelParams.SetContext(context.Background())
	_, ocErr := c.GuildConfigService.Client.ServerOutputChannels.DeleteServerOutputChannel(outputChannelParams, c.GuildConfigService.Auth)
	if ocErr != nil {
		ErrorOutput(ctx, s, c.Config, command, mc.Content, mc.ChannelID, Error{
			Message: "Failed to unset server output",
			Err:     ocErr,
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	channel, gcErr := discordapi.GetChannel(s, existingOutputChannel.ChannelID)
	if gcErr != nil {
		embeddableFields = append(embeddableFields, &UnsetOutputChannelOutput{
			Server: server,
		})
		SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
		return
	}

	embeddableFields = append(embeddableFields, &UnsetOutputChannelOutput{
		Server:  server,
		Channel: channel,
	})
	SuccessOutput(ctx, s, c.Config, mc.ChannelID, command, embeddableFields, embeddableErrors)
}

// parseUnsetOutputChannelCommand func
func parseUnsetOutputChannelCommand(command configs.Command, mc *discordgo.MessageCreate) (*UnsetOutputChannelCommand, *Error) {
	splitContent := strings.Split(mc.Content, " ")

	if len(splitContent)-1 < command.MinArgs || len(splitContent)-1 > command.MaxArgs {
		return nil, &Error{
			Message: fmt.Sprintf("Command given %d arguments, expects %d to %d arguments.", len(splitContent)-1, command.MinArgs, command.MaxArgs),
			Err:     errors.New("invalid number of arguments"),
		}
	}

	serverIDInt, sidErr := strconv.ParseInt(splitContent[1], 10, 64)
	if sidErr != nil {
		return nil, &Error{
			Message: "Invalid Server ID",
			Err:     sidErr,
		}
	}

	return &UnsetOutputChannelCommand{
		Params: UnsetOutputChannelCommandParams{
			ServerID: serverIDInt,
		},
	}, nil
}

// ConvertToEmbedField for Error struct
func (o *UnsetOutputChannelOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""
	if o.Channel != nil {
		fieldVal += "Removed: " + o.Channel.Name
	} else {
		fieldVal += "Channel previously deleted"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Unset Status Output for Server",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
