package interactions

import (
	"context"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/google/uuid"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/interactions/commands"
	"gitlab.com/BIC_Dev/status-bot-v2/interactions/messages"
	"gitlab.com/BIC_Dev/status-bot-v2/models"
	"gitlab.com/BIC_Dev/status-bot-v2/services/guildconfigservice"
	"gitlab.com/BIC_Dev/status-bot-v2/services/nitradoservice"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/cache"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// Interactions struct
type Interactions struct {
	Session            *discordgo.Session
	Config             *configs.Config
	Cache              *cache.Cache
	GuildConfigService *guildconfigservice.GuildConfigService
	NitradoService     *nitradoservice.NitradoService
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
	Code    int    `json:"code"`
}

// Error func
func (ie *Error) Error() string {
	return ie.Err.Error()
}

// SetupHandlers func
func (i *Interactions) SetupHandlers() {
	i.Session.AddHandler(i.MessageCreate)
	i.Session.AddHandler(i.MessageReaction)
}

// MessageCreate func
func (i *Interactions) MessageCreate(s *discordgo.Session, mc *discordgo.MessageCreate) {
	requestID := uuid.New()

	ctx := context.Background()
	ctx = logging.AddValues(ctx, zap.String("request_id", requestID.String()), zap.String("scope", logging.GetFuncName()))

	// Ignore message if user is self
	if mc.Author.ID == s.State.User.ID {
		return
	}

	// Ignore message if user is a bot
	if mc.Author.Bot {
		return
	}

	// Check if the message is a command
	if strings.HasPrefix(strings.ToLower(mc.Content), i.Config.Bot.Prefix) {
		commands := commands.Commands{
			Session:            i.Session,
			Config:             i.Config,
			Cache:              i.Cache,
			GuildConfigService: i.GuildConfigService,
			NitradoService:     i.NitradoService,
		}
		commands.Factory(ctx, s, mc)
		return
	}

	var cms *models.CommandMessageStep
	cacheKey := cms.CacheKey(i.Config.CacheSettings.CommandMessageStep.Base, mc.Author.ID, mc.ChannelID)
	cErr := i.Cache.GetStruct(ctx, cacheKey, &cms)
	if cErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", cErr.Err), zap.String("error_message", cErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	if cms != nil {
		messages := messages.Messages{
			Session:            i.Session,
			Config:             i.Config,
			Cache:              i.Cache,
			GuildConfigService: i.GuildConfigService,
			NitradoService:     i.NitradoService,
		}
		messages.Factory(s, mc, cms)
		return
	}
}

// MessageReaction func
func (i *Interactions) MessageReaction(s *discordgo.Session, mra *discordgo.MessageReactionAdd) {
	requestID := uuid.New()

	ctx := context.Background()
	ctx = logging.AddValues(ctx, zap.String("request_id", requestID.String()), zap.String("scope", logging.GetFuncName()))

	// Ignore reaction if user is self
	if mra.UserID == s.State.User.ID {
		return
	}

	// TODO: Needs additional data. For example, if it's a BAN command reaction, who are we trying to ban? Maybe a data cache key to get the info stored separately?
	var cmr *models.CommandMessageReaction
	cacheKey := cmr.CacheKey(i.Config.CacheSettings.CommandMessageReaction.Base, mra.MessageID)
	cErr := i.Cache.GetStruct(ctx, cacheKey, &cmr)
	if cErr != nil {
		// TODO: Log Error
	} else if cmr != nil {
		// TODO: This is a reaction
	}
}
