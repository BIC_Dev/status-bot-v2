package runners

import (
	"context"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/status-bot-v2/configs"
	"gitlab.com/BIC_Dev/status-bot-v2/models"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/services/guildconfigservice"
	"gitlab.com/BIC_Dev/status-bot-v2/services/nitradoservice"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/cache"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// Runner struct
type Runner struct {
	Session            *discordgo.Session
	Config             *configs.Config
	Cache              *cache.Cache
	GuildConfigService *guildconfigservice.GuildConfigService
	NitradoService     *nitradoservice.NitradoService
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
}

// Error func
func (e *Error) Error() string {
	return e.Err.Error()
}

// ConvertToEmbedField for Error struct
func (e *Error) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	return &discordgo.MessageEmbedField{
		Name:   e.Message,
		Value:  e.Error(),
		Inline: false,
	}, nil
}

// StartRunners func
func (r *Runner) StartRunners() {
	go r.StatusRunner(r.Config.Runners.Status.Delay)
	go r.ServicesRunner(r.Config.Runners.Services.Delay)
}

// SuccessOutput func
func (r *Runner) SuccessOutput(ctx context.Context, session *discordgo.Session, c *configs.Config, channelID string, embeddableFields []discordapi.EmbeddableField, embeddableErrors []discordapi.EmbeddableField) ([]*discordgo.Message, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var statusOutputChannelMessages *models.StatusOutputChannelMessages
	cacheKey := statusOutputChannelMessages.CacheKey(r.Config.CacheSettings.StatusOutputChannelMessages.Base, channelID)
	stcErr := r.Cache.GetStruct(ctx, cacheKey, &statusOutputChannelMessages)
	if stcErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", stcErr.Err), zap.String("error_message", stcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: stcErr.Message,
			Err:     stcErr.Err,
		}
	}

	params := discordapi.EmbeddableParams{
		Title:       "Server Status",
		Description: "\u200b",
		Color:       c.Bot.OkColor,
		TitleURL:    c.Bot.DocumentationURL,
		Footer:      "Updated",
	}

	if len(embeddableErrors) > 0 {
		params.Color = c.Bot.WarnColor
	}

	combinedFields := append(embeddableFields, embeddableErrors...)
	embeds := discordapi.CreateEmbeds(params, combinedFields)

	var messages []*discordgo.Message
	var modelMessages []models.Message
	for key, embed := range embeds {
		if statusOutputChannelMessages != nil {
			if key >= len(statusOutputChannelMessages.Messages) {
				message, err := discordapi.SendMessage(session, channelID, nil, &embed)
				if err != nil {
					ctx = logging.AddValues(ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Message), zap.Int("status_code", err.Code))
					logger := logging.Logger(ctx)
					logger.Error("error_log")

					return nil, &Error{
						Message: err.Message,
						Err:     err.Err,
					}
				}
				messages = append(messages, message)
				modelMessages = append(modelMessages, models.Message{
					ID: message.ID,
					Channel: models.Channel{
						ID: message.ChannelID,
					},
				})
			} else {
				message, err := discordapi.EditMessage(session, channelID, statusOutputChannelMessages.Messages[key].ID, nil, &embed)
				if err != nil {
					ctx = logging.AddValues(ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Message), zap.Int("status_code", err.Code))
					logger := logging.Logger(ctx)
					logger.Error("error_log")

					if err.Code == 10008 {
						newmessage, nerr := discordapi.SendMessage(session, channelID, nil, &embed)
						if nerr != nil {
							ctx = logging.AddValues(ctx, zap.NamedError("error", nerr.Err), zap.String("error_message", nerr.Message), zap.Int("status_code", nerr.Code))
							logger := logging.Logger(ctx)
							logger.Error("error_log")

							return nil, &Error{
								Message: nerr.Message,
								Err:     nerr.Err,
							}
						}
						messages = append(messages, newmessage)
						modelMessages = append(modelMessages, models.Message{
							ID: newmessage.ID,
							Channel: models.Channel{
								ID: newmessage.ChannelID,
							},
						})
					}
				} else {
					messages = append(messages, message)
					modelMessages = append(modelMessages, models.Message{
						ID: message.ID,
						Channel: models.Channel{
							ID: message.ChannelID,
						},
					})
				}
			}
		}
	}

	setErr := r.Cache.SetStruct(ctx, cacheKey, &models.StatusOutputChannelMessages{
		Messages: modelMessages,
		Channel: models.Channel{
			ID: channelID,
		},
	}, r.Config.CacheSettings.StatusOutputChannelMessages.TTL)
	if setErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", setErr.Err), zap.String("error_message", setErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: stcErr.Message,
			Err:     stcErr.Err,
		}
	}

	if statusOutputChannelMessages != nil {
		for _, prevMessage := range statusOutputChannelMessages.Messages {
			foundMessage := false
			for _, newMessage := range modelMessages {
				if newMessage.ID == prevMessage.ID {
					foundMessage = true
					break
				}
			}

			if !foundMessage {
				dmErr := discordapi.DeleteMessage(r.Session, channelID, prevMessage.ID)
				if dmErr != nil {
					ctx = logging.AddValues(ctx, zap.NamedError("error", dmErr.Err), zap.String("error_message", dmErr.Message))
					logger := logging.Logger(ctx)
					logger.Error("error_log")

					if dmErr.Code == 10008 {
						// TODO: Handle message not found by logging
					} else {
						return nil, &Error{
							Message: dmErr.Message,
							Err:     dmErr.Err,
						}
					}
				}
			}
		}
	}

	return messages, nil
}
