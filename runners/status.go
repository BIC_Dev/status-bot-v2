package runners

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gammazero/workerpool"
	"github.com/google/uuid"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guilds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	nitrado_service_v2_client "gitlab.com/BIC_Dev/nitrado-service-v2-client"
	"gitlab.com/BIC_Dev/status-bot-v2/models"
	"gitlab.com/BIC_Dev/status-bot-v2/services/discordapi"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// StatusResponse struct
type StatusResponse struct {
	GameserverResponse  *nitrado_service_v2_client.GetGameserverResponse
	Server              *gcscmodels.Server
	OutputChannel       *gcscmodels.ServerOutputChannel
	Runtime             *models.ServerRuntime
	Players             *models.ServerPlayers
	GuildOutputChannels []*gcscmodels.GuildOutputChannel
}

// StatusError struct
type StatusError struct {
	Server        *gcscmodels.Server
	OutputChannel *gcscmodels.ServerOutputChannel
	Err           *Error
}

// ServerWithOutputChannel struct
type ServerWithOutputChannel struct {
	Server        *gcscmodels.Server
	OutputChannel *gcscmodels.ServerOutputChannel
}

// ServersWithGuildOutputChannels struct
type ServersWithGuildOutputChannels struct {
	ServersWithOutputChannel []ServerWithOutputChannel
	GuildOutputChannels      []*gcscmodels.GuildOutputChannel
}

func (s *StatusError) Error() string {
	return s.Err.Error()
}

// StatusRunner func
func (r *Runner) StatusRunner(delay time.Duration) {
	ctx := context.Background()
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("runner", "status"),
	)

	if delay != 0 {
		time.Sleep(time.Second * delay)
	}

	ticker := time.NewTicker(r.Config.Runners.Status.Frequency * time.Second)

	wp := workerpool.New(r.Config.Runners.Status.Workers)

	for range ticker.C {
		// If workers have not finished last batch, skip this run
		if wp.WaitingQueueSize() > 0 {
			continue
		}

		requestID := uuid.New()
		sCtx := logging.AddValues(ctx, zap.String("request_id", requestID.String()))

		guilds, ggErr := r.GetAllGuildsForStatus(sCtx)
		if ggErr != nil {
			newCTX := logging.AddValues(sCtx,
				zap.NamedError("error", ggErr.Err),
				zap.String("error_message", ggErr.Message),
			)
			logger := logging.Logger(newCTX)
			logger.Error("error_log")
			continue
		}

		for _, guild := range guilds {
			if !guild.Enabled {
				continue
			}

			gCtx := logging.AddValues(sCtx, zap.String("guild_id", guild.ID))

			servers, ggsErr := r.GetGuildServersForStatus(gCtx, guild)
			if ggsErr != nil {
				// newCTX := logging.AddValues(gCtx,
				// 	zap.NamedError("error", ggsErr.Err),
				// 	zap.String("error_message", ggsErr.Message),
				// )
				// logger := logging.Logger(newCTX)
				// logger.Error("error_log")
				continue
			}

			gameserverResponseChannel := make(chan StatusResponse, len(servers.ServersWithOutputChannel))
			errorChannel := make(chan StatusError, len(servers.ServersWithOutputChannel))

			go r.HandleStatusResponse(gCtx, len(servers.ServersWithOutputChannel), servers.GuildOutputChannels, gameserverResponseChannel, errorChannel)

			for _, server := range servers.ServersWithOutputChannel {
				var aServer gcscmodels.Server = *server.Server
				var outputChannel gcscmodels.ServerOutputChannel = *server.OutputChannel
				wp.Submit(func() {
					r.GetGameserverInfo(gCtx, aServer, outputChannel, gameserverResponseChannel, errorChannel)
				})
			}
		}
	}
}

// GetGuildServersForStatus func
func (r *Runner) GetGuildServersForStatus(ctx context.Context, guild *gcscmodels.Guild) (*ServersWithGuildOutputChannels, *Error) {
	ctx = logging.AddValues(ctx, zap.String("guild_id", guild.ID))

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = guild.ID
	guildFeedParams.GuildID = guild.ID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := r.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, r.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			return nil, &Error{
				Message: "guild feed not found for guild",
				Err:     notfound,
			}
		}

		return nil, &Error{
			Message: "request failed for guild feed",
			Err:     gfErr,
		}
	}

	if guildFeed.Payload == nil {
		return nil, &Error{
			Message: "guild feed does not have a payload",
			Err:     errors.New("guild feed nil payload"),
		}
	}

	if guildFeed.Payload.Guild == nil {
		return nil, &Error{
			Message: "guild feed does not have a guild",
			Err:     errors.New("guild feed nil guild"),
		}
	}

	if guildFeed.Payload.Guild.Enabled == false {
		return nil, &Error{
			Message: "guild is not enabled",
			Err:     errors.New("guild is not enabled"),
		}
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		return nil, &Error{
			Message: "guild feed does not have guild services",
			Err:     errors.New("guild feed nil guild services"),
		}
	}

	hasGuildService := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == r.Config.Bot.GuildService {
			hasGuildService = true
			if guildService.Enabled == false {
				return nil, &Error{
					Message: "guild service is not enabled",
					Err:     errors.New("guild service not enabled"),
				}
			}
		}
	}

	if !hasGuildService {
		return nil, &Error{
			Message: "guild service for status bot v2 does not exist",
			Err:     errors.New("guild does not have this service"),
		}
	}

	if guildFeed.Payload.Guild.Servers == nil {
		return nil, &Error{
			Message: "guild has no servers",
			Err:     errors.New("guild feed nil servers"),
		}
	}

	var servers ServersWithGuildOutputChannels
	for _, server := range guildFeed.Payload.Guild.Servers {
		if !server.Enabled {
			continue
		}

		if server.ServerOutputChannels == nil {
			continue
		}

		for _, oc := range server.ServerOutputChannels {
			if oc.OutputChannelTypeID == "status-output" {
				if oc.OutputChannelType.Enabled && oc.Enabled {
					servers.ServersWithOutputChannel = append(servers.ServersWithOutputChannel, ServerWithOutputChannel{
						Server:        server,
						OutputChannel: oc,
					})
				}
			}
		}
	}

	if guildFeed.Payload.Guild.GuildOutputChannels != nil {
		servers.GuildOutputChannels = guildFeed.Payload.Guild.GuildOutputChannels
	}

	return &servers, nil
}

// GetGameserverInfo func
func (r *Runner) GetGameserverInfo(ctx context.Context, server gcscmodels.Server, outputChannel gcscmodels.ServerOutputChannel, statusResponse chan StatusResponse, statusErr chan StatusError) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.Uint64("server_id", server.ID),
	)

	if server.NitradoToken == nil {
		statusErr <- StatusError{
			Server:        &server,
			OutputChannel: &outputChannel,
			Err: &Error{
				Message: "Missing Nitrado Token for server",
				Err:     errors.New("nil server nitrado token"),
			},
		}
		return
	}

	resp, err := r.NitradoService.Client.GetGameserverByID(server.NitradoToken.Token, fmt.Sprint(server.NitradoID), false)
	if err != nil {
		statusErr <- StatusError{
			Server:        &server,
			OutputChannel: &outputChannel,
			Err: &Error{
				Message: err.Message(),
				Err:     errors.New(err.Error()),
			},
		}
		return
	}

	if resp.Error != "" {
		statusErr <- StatusError{
			Server:        &server,
			OutputChannel: &outputChannel,
			Err: &Error{
				Message: "Error retrieving gameserver from nsv2",
				Err:     errors.New(resp.Error),
			},
		}
		return
	}

	sr := StatusResponse{
		GameserverResponse: &resp,
		Server:             &server,
		OutputChannel:      &outputChannel,
	}

	if resp.Data.Gameserver.Status != "started" {
		statusResponse <- sr
		return
	}

	if resp.Data.Gameserver.Query.PlayerMax != 0 {
		var serverPlayers *models.ServerPlayers = &models.ServerPlayers{
			Server: models.Server{
				ID: server.ID,
			},
			PlayersCurrent: resp.Data.Gameserver.Query.PlayerCurrent,
			PlayersMax:     resp.Data.Gameserver.Query.PlayerMax,
		}
		sr.Players = serverPlayers

		cacheKey := serverPlayers.CacheKey(r.Config.CacheSettings.ServerPlayers.Base, int(server.ID))
		setErr := r.Cache.SetStruct(ctx, cacheKey, &serverPlayers, r.Config.CacheSettings.ServerPlayers.TTL)
		if setErr != nil {
			statusErr <- StatusError{
				Server:        &server,
				OutputChannel: &outputChannel,
				Err: &Error{
					Message: "Error caching player data",
					Err:     setErr.Err,
				},
			}
			return
		}
	} else {
		var serverPlayers *models.ServerPlayers
		cacheKey := serverPlayers.CacheKey(r.Config.CacheSettings.ServerPlayers.Base, int(server.ID))
		getErr := r.Cache.GetStruct(ctx, cacheKey, &serverPlayers)
		if getErr != nil {
			statusErr <- StatusError{
				Server:        &server,
				OutputChannel: &outputChannel,
				Err: &Error{
					Message: "Error getting cached player data",
					Err:     getErr.Err,
				},
			}
			return
		}

		sr.Players = serverPlayers
	}

	var serverRuntime *models.ServerRuntime
	cacheKey := serverRuntime.CacheKey(r.Config.CacheSettings.ServerRuntime.Base, server.ID)
	getErr := r.Cache.GetStruct(ctx, cacheKey, &serverRuntime)
	if getErr != nil {
		statusErr <- StatusError{
			Server:        &server,
			OutputChannel: &outputChannel,
			Err: &Error{
				Message: "Error getting cached server runtime data",
				Err:     getErr.Err,
			},
		}
		return
	}
	sr.Runtime = serverRuntime

	statusResponse <- sr

	return
}

// HandleStatusResponse func
func (r *Runner) HandleStatusResponse(ctx context.Context, statuses int, guildOutputChannels []*gcscmodels.GuildOutputChannel, statusResponse chan StatusResponse, statusErr chan StatusError) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	count := 0
	var responses []StatusResponse
	var errs []StatusError

	var timer *time.Timer = time.NewTimer(180 * time.Second)

Loop:
	for {
		if count == statuses {
			break
		}

		select {
		case response := <-statusResponse:
			count++
			responses = append(responses, response)
		case err := <-statusErr:
			count++
			errs = append(errs, err)
		case <-timer.C:
			break Loop
		}
	}

	var statusOutputs map[string][]discordapi.EmbeddableField = make(map[string][]discordapi.EmbeddableField, 0)
	var statusErrors map[string][]discordapi.EmbeddableField = make(map[string][]discordapi.EmbeddableField, 0)
	var channels map[string]int = make(map[string]int, 0)

	sort.SliceStable(responses, func(i, j int) bool {
		return responses[i].Server.Name < responses[j].Server.Name
	})

	sort.SliceStable(errs, func(i, j int) bool {
		return errs[i].Server.Name < errs[j].Server.Name
	})

	totalServers := len(responses) + len(errs)
	onlineServers := 0
	onlinePlayers := 0
	var totalRuntime int64 = 0
	var averageRuntime float64

	for _, response := range responses {
		var aResponse StatusResponse = response

		if response.GameserverResponse.Data.Gameserver.Status == "started" {
			onlineServers++
		}

		if response.Players != nil {
			onlinePlayers += response.Players.PlayersCurrent
		}

		if response.Runtime != nil {
			runtimeRemaining := response.Runtime.SuspendTimestamp - time.Now().Unix()
			if runtimeRemaining > 0 {
				totalRuntime += runtimeRemaining
			}
		}

		if _, ok := statusOutputs[response.OutputChannel.ChannelID]; !ok {
			if _, exist := channels[response.OutputChannel.ChannelID]; !exist {
				channels[response.OutputChannel.ChannelID] = 0
			}

			statusOutputs[response.OutputChannel.ChannelID] = []discordapi.EmbeddableField{
				&aResponse,
			}
			continue
		}

		statusOutputs[response.OutputChannel.ChannelID] = append(statusOutputs[response.OutputChannel.ChannelID], &aResponse)
	}

	if totalServers == 0 {
		averageRuntime = 0
	} else {
		averageRuntime = (float64(totalRuntime) / float64(totalServers)) / float64(60*60*24)
	}

	for _, err := range errs {
		var anErr StatusError = err

		if _, ok := statusErrors[err.OutputChannel.ChannelID]; !ok {
			if _, exist := channels[err.OutputChannel.ChannelID]; !exist {
				channels[err.OutputChannel.ChannelID] = 0
			}

			statusErrors[err.OutputChannel.ChannelID] = []discordapi.EmbeddableField{
				&anErr,
			}
			continue
		}

		statusErrors[err.OutputChannel.ChannelID] = append(statusErrors[err.OutputChannel.ChannelID], &anErr)
	}

	for channel := range channels {
		var embeddableFields []discordapi.EmbeddableField
		var embeddableErrors []discordapi.EmbeddableField

		if val, ok := statusOutputs[channel]; ok {
			embeddableFields = val
		}

		if val, ok := statusErrors[channel]; ok {
			embeddableErrors = val
		}

		go r.SuccessOutput(ctx, r.Session, r.Config, channel, embeddableFields, embeddableErrors)
	}

	if guildOutputChannels == nil || len(guildOutputChannels) == 0 {
		return
	}

	for _, channel := range guildOutputChannels {
		switch channel.OutputChannelTypeID {
		case "status-players-display":
			go r.WritePlayersDisplay(ctx, channel, onlinePlayers)
		case "status-runtime-display":
			go r.WriteRuntimeDisplay(ctx, channel, averageRuntime)
		case "status-servers-display":
			go r.WriteServersDisplay(ctx, channel, onlineServers)
		}
	}
}

// ConvertToEmbedField for Error struct
func (sr *StatusResponse) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if sr.Players != nil {
		fieldVal += fmt.Sprintf("**Players:** %d/%d\n", sr.Players.PlayersCurrent, sr.Players.PlayersMax)
	} else {
		fieldVal += fmt.Sprintf("**Players:** %d/%d\n", 0, 0)
	}

	if sr.Server.BoostSetting != nil {
		if sr.Server.BoostSetting.Code != "" {
			fieldVal += fmt.Sprintf("**Boost Code:** %s\n", sr.Server.BoostSetting.Code)
		}
	}

	if sr.Runtime != nil {
		secondsRemaining := sr.Runtime.SuspendTimestamp - time.Now().Unix()
		var runtime float64
		if secondsRemaining <= 0 {
			runtime = 0
		} else {
			runtime = float64(secondsRemaining) / float64(60*60*24)
		}
		fieldVal += fmt.Sprintf("**Runtime:** %.1f days", runtime)
	}

	statusSymbol := ""
	switch sr.GameserverResponse.Data.Gameserver.Status {
	case "started":
		statusSymbol = "🟢"
	case "stopping":
		statusSymbol = "🔴"
	case "restarting":
		statusSymbol = "🔴"
	case "stopped":
		statusSymbol = "🔴"
	case "suspended":
		statusSymbol = "🔴"
	default:
		statusSymbol = "🔴"
	}

	return &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s %s", statusSymbol, sr.Server.Name),
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for StatusError struct
func (s *StatusError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("**Message:** %s\n**Error:** %s", s.Err.Message, s.Err.Error())

	return &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s %s", "🟠", s.Server.Name),
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// GetAllGuildsForStatus func
func (r *Runner) GetAllGuildsForStatus(ctx context.Context) ([]*gcscmodels.Guild, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	guildParams := guilds.NewGetAllGuildsParamsWithTimeout(20)
	guildParams.SetContext(context.Background())
	allGuilds, agErr := r.GuildConfigService.Client.Guilds.GetAllGuilds(guildParams, r.GuildConfigService.Auth)
	if agErr != nil {
		return nil, &Error{
			Message: "failed to retreive all guilds from guild config service",
			Err:     agErr,
		}
	}

	if allGuilds.Payload == nil {
		return nil, &Error{
			Message: "all guilds request has nil payload",
			Err:     errors.New("nil payload"),
		}
	}

	if allGuilds.Payload.Guilds == nil {
		return nil, &Error{
			Message: "all guilds request has nil guilds",
			Err:     errors.New("nil guilds"),
		}
	}

	return allGuilds.Payload.Guilds, nil
}

// WritePlayersDisplay func
func (r *Runner) WritePlayersDisplay(ctx context.Context, channel *gcscmodels.GuildOutputChannel, playersCurrent int) (*discordgo.Channel, *Error) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("channel_id", channel.ChannelID),
		zap.String("channel_type", channel.OutputChannelTypeID),
	)

	var cnc *models.ChannelNameChanges
	cacheKey := cnc.CacheKey(r.Config.CacheSettings.ChannelNameChanges.Base, channel.ChannelID)
	stcErr := r.Cache.GetStruct(ctx, cacheKey, &cnc)
	if stcErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", stcErr.Err), zap.String("error_message", stcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: stcErr.Message,
			Err:     stcErr.Err,
		}
	}

	if cnc != nil {
		return nil, &Error{
			Message: "stopped writing players display update due to rate limit",
			Err:     errors.New("rate limit"),
		}
	}

	setCacheErr := r.Cache.SetStruct(ctx, cacheKey, &models.ChannelNameChanges{
		Channel: models.Channel{
			ID: channel.ChannelID,
		},
	}, r.Config.CacheSettings.ChannelNameChanges.TTL)
	if setCacheErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", setCacheErr.Err), zap.String("error_message", setCacheErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: setCacheErr.Message,
			Err:     setCacheErr.Err,
		}
	}

	channelEdit := &discordgo.ChannelEdit{
		Name: fmt.Sprintf("Players Online: %d", playersCurrent),
	}

	upChannel, upErr := discordapi.UpdateChannel(r.Session, channel.ChannelID, channelEdit)
	if upErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", upErr.Err), zap.String("error_message", upErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		if upErr.Code == 10003 {
			// TODO: Delete GuildOutputChannel
		}
		return nil, &Error{
			Message: upErr.Message,
			Err:     upErr.Err,
		}
	}

	return upChannel, nil
}

// WriteRuntimeDisplay func
func (r *Runner) WriteRuntimeDisplay(ctx context.Context, channel *gcscmodels.GuildOutputChannel, averageRuntime float64) (*discordgo.Channel, *Error) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("channel_id", channel.ChannelID),
		zap.String("channel_type", channel.OutputChannelTypeID),
	)

	var cnc *models.ChannelNameChanges
	cacheKey := cnc.CacheKey(r.Config.CacheSettings.ChannelNameChanges.Base, channel.ChannelID)
	stcErr := r.Cache.GetStruct(ctx, cacheKey, &cnc)
	if stcErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", stcErr.Err), zap.String("error_message", stcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: stcErr.Message,
			Err:     stcErr.Err,
		}
	}

	if cnc != nil {
		return nil, &Error{
			Message: "stopped writing runtime display update due to rate limit",
			Err:     errors.New("rate limit"),
		}
	}

	setCacheErr := r.Cache.SetStruct(ctx, cacheKey, &models.ChannelNameChanges{
		Channel: models.Channel{
			ID: channel.ChannelID,
		},
	}, r.Config.CacheSettings.ChannelNameChanges.TTL)
	if setCacheErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", setCacheErr.Err), zap.String("error_message", setCacheErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: setCacheErr.Message,
			Err:     setCacheErr.Err,
		}
	}

	channelEdit := &discordgo.ChannelEdit{
		Name: fmt.Sprintf("Avg Runtime: %.1f days", averageRuntime),
	}

	upChannel, upErr := discordapi.UpdateChannel(r.Session, channel.ChannelID, channelEdit)
	if upErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", upErr.Err), zap.String("error_message", upErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		if upErr.Code == 10003 {
			// TODO: Delete GuildOutputChannel
		}
		return nil, &Error{
			Message: upErr.Message,
			Err:     upErr.Err,
		}
	}

	return upChannel, nil
}

// WriteServersDisplay func
func (r *Runner) WriteServersDisplay(ctx context.Context, channel *gcscmodels.GuildOutputChannel, onlineServers int) (*discordgo.Channel, *Error) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("channel_id", channel.ChannelID),
		zap.String("channel_type", channel.OutputChannelTypeID),
	)

	var cnc *models.ChannelNameChanges
	cacheKey := cnc.CacheKey(r.Config.CacheSettings.ChannelNameChanges.Base, channel.ChannelID)
	stcErr := r.Cache.GetStruct(ctx, cacheKey, &cnc)
	if stcErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", stcErr.Err), zap.String("error_message", stcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: stcErr.Message,
			Err:     stcErr.Err,
		}
	}

	if cnc != nil {
		return nil, &Error{
			Message: "stopped writing servers display update due to rate limit",
			Err:     errors.New("rate limit"),
		}
	}

	setCacheErr := r.Cache.SetStruct(ctx, cacheKey, &models.ChannelNameChanges{
		Channel: models.Channel{
			ID: channel.ChannelID,
		},
	}, r.Config.CacheSettings.ChannelNameChanges.TTL)
	if setCacheErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", setCacheErr.Err), zap.String("error_message", setCacheErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return nil, &Error{
			Message: setCacheErr.Message,
			Err:     setCacheErr.Err,
		}
	}

	channelEdit := &discordgo.ChannelEdit{
		Name: fmt.Sprintf("Servers Online: %d", onlineServers),
	}

	upChannel, upErr := discordapi.UpdateChannel(r.Session, channel.ChannelID, channelEdit)
	if upErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", upErr.Err), zap.String("error_message", upErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		if upErr.Code == 10003 {
			// TODO: Delete GuildOutputChannel
		}
		return nil, &Error{
			Message: upErr.Message,
			Err:     upErr.Err,
		}
	}

	return upChannel, nil
}
