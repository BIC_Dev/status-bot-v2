package runners

import (
	"context"
	"errors"
	"time"

	"github.com/gammazero/workerpool"
	"github.com/google/uuid"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guilds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/status-bot-v2/models"
	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
)

// ServersAndToken struct
type ServersAndToken struct {
	Servers []*gcscmodels.Server
	Token   *gcscmodels.NitradoToken
}

// ServicesRunner func
func (r *Runner) ServicesRunner(delay time.Duration) {
	ctx := context.Background()
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("runner", "services"),
	)

	if delay != 0 {
		time.Sleep(time.Second * delay)
	}

	ticker := time.NewTicker(r.Config.Runners.Services.Frequency * time.Second)

	wp := workerpool.New(r.Config.Runners.Services.Workers)

	for range ticker.C {
		// If workers have not finished last batch, skip this run
		if wp.WaitingQueueSize() > 0 {
			continue
		}

		requestID := uuid.New()
		sCtx := logging.AddValues(ctx, zap.String("request_id", requestID.String()))

		guilds, ggErr := r.GetAllGuildsForServices(sCtx)
		if ggErr != nil {
			newCTX := logging.AddValues(sCtx,
				zap.NamedError("error", ggErr.Err),
				zap.String("error_message", ggErr.Message),
			)
			logger := logging.Logger(newCTX)
			logger.Error("error_log")
			continue
		}

		for _, guild := range guilds {
			if !guild.Enabled {
				continue
			}

			var aGuild gcscmodels.Guild = *guild

			gCtx := logging.AddValues(sCtx, zap.String("guild_id", aGuild.ID))

			serversAndTokens, ggsErr := r.GetGuildTokensForService(gCtx, &aGuild)
			if ggsErr != nil {
				// newCTX := logging.AddValues(gCtx,
				// 	zap.NamedError("error", ggsErr.Err),
				// 	zap.String("error_message", ggsErr.Message),
				// )
				// logger := logging.Logger(newCTX)
				// logger.Error("error_log")
				continue
			}
			if len(serversAndTokens) == 0 {
				continue
			}

			for _, st := range serversAndTokens {
				var anST ServersAndToken = st
				wp.Submit(func() {
					r.GetServiceInfo(gCtx, &aGuild, anST)
				})
			}
		}
	}

}

// GetGuildTokensForService func
func (r *Runner) GetGuildTokensForService(ctx context.Context, guild *gcscmodels.Guild) ([]ServersAndToken, *Error) {
	ctx = logging.AddValues(ctx, zap.String("guild_id", guild.ID))

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = guild.ID
	guildFeedParams.GuildID = guild.ID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := r.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, r.GuildConfigService.Auth)

	if gfErr != nil {
		if notfound, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			return nil, &Error{
				Message: "guild feed not found for guild",
				Err:     notfound,
			}
		}

		return nil, &Error{
			Message: "request failed for guild feed",
			Err:     gfErr,
		}
	}

	if guildFeed.Payload == nil {
		return nil, &Error{
			Message: "guild feed does not have a payload",
			Err:     errors.New("guild feed nil payload"),
		}
	}

	if guildFeed.Payload.Guild == nil {
		return nil, &Error{
			Message: "guild feed does not have a guild",
			Err:     errors.New("guild feed nil guild"),
		}
	}

	if guildFeed.Payload.Guild.Enabled == false {
		return nil, &Error{
			Message: "guild is not enabled",
			Err:     errors.New("guild is not enabled"),
		}
	}

	if guildFeed.Payload.Guild.GuildServices == nil {
		return nil, &Error{
			Message: "guild feed does not have guild services",
			Err:     errors.New("guild feed nil guild services"),
		}
	}

	hasGuildService := false
	for _, guildService := range guildFeed.Payload.Guild.GuildServices {
		if guildService.Name == r.Config.Bot.GuildService {
			hasGuildService = true
			if guildService.Enabled == false {
				return nil, &Error{
					Message: "guild service is not enabled",
					Err:     errors.New("guild service not enabled"),
				}
			}
		}
	}

	if !hasGuildService {
		return nil, &Error{
			Message: "guild service for status bot v2 does not exist",
			Err:     errors.New("guild does not have this service"),
		}
	}

	if guildFeed.Payload.Guild.Servers == nil {
		return nil, &Error{
			Message: "guild has no servers",
			Err:     errors.New("guild feed nil servers"),
		}
	}

	if guildFeed.Payload.Guild.NitradoTokens == nil {
		return nil, &Error{
			Message: "guild has no nitrado tokens",
			Err:     errors.New("guild feed nil nitrado tokens"),
		}
	}

	var serversAndToken []ServersAndToken

	for _, token := range guildFeed.Payload.Guild.NitradoTokens {
		var st ServersAndToken = ServersAndToken{
			Token: token,
		}

		for _, server := range guildFeed.Payload.Guild.Servers {
			if server.NitradoTokenID == token.ID {
				st.Servers = append(st.Servers, server)
			}
		}

		serversAndToken = append(serversAndToken, st)
	}

	return serversAndToken, nil
}

// GetAllGuildsForServices func
func (r *Runner) GetAllGuildsForServices(ctx context.Context) ([]*gcscmodels.Guild, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	guildParams := guilds.NewGetAllGuildsParamsWithTimeout(20)
	guildParams.SetContext(context.Background())
	allGuilds, agErr := r.GuildConfigService.Client.Guilds.GetAllGuilds(guildParams, r.GuildConfigService.Auth)
	if agErr != nil {
		return nil, &Error{
			Message: "failed to retreive all guilds from guild config service",
			Err:     agErr,
		}
	}

	if allGuilds.Payload == nil {
		return nil, &Error{
			Message: "all guilds request has nil payload",
			Err:     errors.New("nil payload"),
		}
	}

	if allGuilds.Payload.Guilds == nil {
		return nil, &Error{
			Message: "all guilds request has nil guilds",
			Err:     errors.New("nil guilds"),
		}
	}

	return allGuilds.Payload.Guilds, nil
}

// GetServiceInfo func
func (r *Runner) GetServiceInfo(ctx context.Context, guild *gcscmodels.Guild, st ServersAndToken) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	resp, err := r.NitradoService.Client.GetServices(st.Token.Token)
	if err != nil {
		ctx = logging.AddValues(ctx,
			zap.NamedError("error", errors.New(err.Error())),
			zap.String("error_message", err.Message()),
		)
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	if resp.Error != "" {
		ctx = logging.AddValues(ctx,
			zap.NamedError("error", errors.New(resp.Error)),
			zap.String("error_message", resp.Message),
		)
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	for _, service := range resp.Services {
		var server *gcscmodels.Server
		for _, aServer := range st.Servers {
			if aServer.NitradoID == int64(service.ID) {
				server = aServer
				break
			}
		}

		if server == nil {
			continue
		}

		var serverRuntime *models.ServerRuntime = &models.ServerRuntime{
			Server: models.Server{
				ID: server.ID,
			},
			SuspendingIn:     int64(service.SuspendingIn),
			DeletingIn:       int64(service.DeletingIn),
			SuspendTimestamp: time.Now().Unix() + int64(service.SuspendingIn),
			DeleteTimestamp:  time.Now().Unix() + int64(service.DeletingIn),
		}
		cacheKey := serverRuntime.CacheKey(r.Config.CacheSettings.ServerRuntime.Base, server.ID)
		setErr := r.Cache.SetStruct(ctx, cacheKey, &serverRuntime, r.Config.CacheSettings.ServerRuntime.TTL)
		if setErr != nil {
			newCtx := logging.AddValues(ctx,
				zap.NamedError("error", setErr.Err),
				zap.String("error_message", setErr.Message),
			)
			logger := logging.Logger(newCtx)
			logger.Error("error_log")
		}
	}
}
