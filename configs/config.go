package configs

import (
	"context"
	"os"
	"time"

	"gitlab.com/BIC_Dev/status-bot-v2/utils/logging"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	Redis struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
		Pool int    `yaml:"pool"`
	} `yaml:"REDIS"`
	NitradoService struct {
		URL string `yaml:"url"`
	} `yaml:"NITRADO_SERVICE"`
	GuildConfigService struct {
		Host     string `yaml:"host"`
		BasePath string `yaml:"base_path"`
	} `yaml:"GUILD_CONFIG_SERVICE"`
	CacheSettings struct {
		ActivationToken             CacheSetting `yaml:"activation_token"`
		CommandMessageReaction      CacheSetting `yaml:"command_message_reaction"`
		CommandMessageStep          CacheSetting `yaml:"command_message_step"`
		StatusOutputChannelMessages CacheSetting `yaml:"status_output_channel_messages"`
		ServerPlayers               CacheSetting `yaml:"server_players"`
		ServerRuntime               CacheSetting `yaml:"server_runtime"`
		NitradoTokenGuild           CacheSetting `yaml:"nitrado_token_guild"`
		ChannelNameChanges          CacheSetting `yaml:"channel_name_changes"`
	} `yaml:"CACHE_SETTINGS"`
	Bot struct {
		Prefix           string `yaml:"prefix"`
		OkColor          int    `yaml:"ok_color"`
		WarnColor        int    `yaml:"warn_color"`
		ErrorColor       int    `yaml:"error_color"`
		DocumentationURL string `yaml:"documentation_url"`
		GuildService     string `yaml:"guild_service"`
	} `yaml:"BOT"`
	Runners struct {
		Status   Runner `yaml:"status"`
		Services Runner `yaml:"services"`
	} `yaml:"RUNNERS"`
	Commands  []Command  `yaml:"COMMANDS"`
	Reactions []Reaction `yaml:"REACTIONS"`
}

// CacheSetting struct
type CacheSetting struct {
	Base    string `yaml:"base"`
	TTL     string `yaml:"ttl"`
	Enabled bool   `yaml:"enabled"`
}

// Command struct
type Command struct {
	Name        string   `yaml:"name"`
	Long        string   `yaml:"long"`
	Short       string   `yaml:"short"`
	Description string   `yaml:"description"`
	MinArgs     int      `yaml:"min_args"`
	MaxArgs     int      `yaml:"max_args"`
	Usage       []string `yaml:"usage"`
	Examples    []string `yaml:"examples"`
	Enabled     bool     `yaml:"enabled"`
}

// Reaction struct
type Reaction struct {
	Name string `yaml:"name"`
	ID   string `yaml:"id"`
}

// Runner struct
type Runner struct {
	Frequency time.Duration `yaml:"frequency"`
	Workers   int           `yaml:"workers"`
	Delay     time.Duration `yaml:"delay"`
	Enabled   bool          `yaml:"enabled"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(ctx context.Context, env string) *Config {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &config
}
