check-swagger:
	which swagger || (GO111MODULE=off go get -u github.com/go-swagger/go-swagger/cmd/swagger)

swagger: 
	swagger generate spec -o /Users/donald/go/src/gitlab.com/BIC_Dev/status-bot-v2/docs/swagger.json --scan-models -w /Users/donald/go/src/gitlab.com/BIC_Dev/status-bot-v2/docs

serve-swagger: 
	swagger serve -F=swagger /Users/donald/go/src/gitlab.com/BIC_Dev/status-bot-v2/docs/swagger.json