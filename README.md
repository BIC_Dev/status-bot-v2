# Status Bot V2

## **Features**

### **Display Server Details in Channel**
#### Display:
- Status (online/offline/restarting)
- Boost code
- Players
- Runtime remaining
#### Update Frequency:
- 5 minutes
#### Commands:
- Set Output Channel `n!setoutput` / `n!so`
- Unset Output Channel `n!unsetoutput` / `n!uso`

### **Display Server Status in Channel Name**
#### Display:
- Status
#### Update Frequency:
- 5 minutes
#### Commands:
- Set Display Channel `n!setdisplay` / `n!sd`
- Unset Display Channel `n!unsetdisplay` / `n!usd`

### **Auto Setup Bot/Servers**
#### Commands:
- Setup `n!setup` / `n!su`
- Add Nitrado Token `n!nitradotoken` / `n!nt`

### **Manage Servers**
#### Commands:
- Show server list `n!servers` / `n!ls`
- Change server name `n!nameserver` / `n!ns`
- Remove server `n!removeserver` / `n!rs`


## TO-DO
### **Set Up Guild Config Service Client**
### **Set Up Nitrado Service V2 Client**
### **Commands**
#### List Servers
#### Name Server
#### Remove Server
- Includes confirmation reaction
#### Set Output Channel
#### Unset Output Channel
#### Set Display Channel
#### Unset Display Channel
#### Add Nitrado Token
- Sends message promting for tokens in DM
#### Setup