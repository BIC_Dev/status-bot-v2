package docs

import (
	"gitlab.com/BIC_Dev/status-bot-v2/viewmodels"
)

// swagger:route POST /activation-tokens ActivationTokens createActivationToken
// Create an activation token
// responses:
//   201: createActivationTokenResponse
//   400: errorResponse
//   500: errorResponse
// security:
//   ApiKey:

// Create activation token response
// swagger:response createActivationTokenResponse
type createActivationTokenResponse struct {
	// in:body
	Body viewmodels.CreateActivationTokenResponse
}
