// Package classification Status Bot V2
//
// Documentation of the Status Bot V2
//
//     Schemes: http
//     BasePath: /status-bot-v2
//     Version: 1.0.0
//     Host: localhost:8083
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - ApiKey
//
//    SecurityDefinitions:
//        ApiKey:
//          type: apiKey
//          in: header
//          name: Service-Token
//
// swagger:meta
package docs

import "gitlab.com/BIC_Dev/status-bot-v2/viewmodels"

// Error response body
// swagger:response errorResponse
type errorResponse struct {
	// in:body
	Body viewmodels.ErrorResponse
}
