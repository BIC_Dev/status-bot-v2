package docs

import "gitlab.com/BIC_Dev/status-bot-v2/viewmodels"

// swagger:route GET /status Status getStatus
// Get status of the service
// responses:
//   200: getStatusResponse

// Status response body
// swagger:response getStatusResponse
type getStatusResponseWrapper struct {
	// in:body
	Body viewmodels.GetStatusResponse
}
